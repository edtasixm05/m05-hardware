# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


###  Virtualization

Topic 102: Linux Installation and Package Management

102.6 Linux as a virtualization guest

330.1 Virtualization conceps teory

330.3 KVM

330.4 Other virtualization solutions

330.5 Libvirt & Related tools

 * Imatges del cloud 
 * Sistemes Live prefabricats
 * Virt-Manager / KVM / libvirt
 * VirtualBox

#### Documentació 

 * [Presentació conceptes de vritualització GM](https://gitlab.com/edtasixm05/m05-hardware/-/blob/master/UF2_2_Virtualization/Presentacio_SMX_4AM02UF1.pdf)
 * [HowTo-ASIX-Virtualization_and_installations.pdf](https://gitlab.com/edtasixm05/m05-hardware/-/blob/master/UF2_1_Installation_and_boot_manager/HowTo-ASIX-Virtualization_and_installations.pdf)
 * [Teoria de virtualització i containers](https://gitlab.com/edtasixm05/m05-hardware/-/blob/master/UF2_2_Virtualization/Teoria_Virtualizacion_Contenedores.pdf) 



