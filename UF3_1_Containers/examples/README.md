# M05 Dockers
## @edt ASIX M05 Curs 2023-2024

### Imatges de Docker

 * **edtasixm05/web23:detach** Servidor web detach.

 * **edtasixm05/net23:detach** Servidor amb serveis de *xinetd* corresponents
   als serveis echo(7), daytime(13) i chargen(19).

 * **edtasixm05/ssh23:detach** Servidor SSH amb un conjunt d'usuaris de prova
   (pere, marta, anna...)


```
docker run --rm --name web.edt.org -h web.edt.org -p 2080:80 -d edtasixm05/web23:latest

docker run --rm --name net.edt.org -h net.edt.org -p 2007:7 -p 2013:13 -p 2019:19 -d edtasixm05/net23:latest 

docker run --rm --name ssh.edt.org -h ssh.edt.org -p 2022:22 -d edtasixm05/ssh23:latets 
```
