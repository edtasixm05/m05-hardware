# Install via HTTP

### Exemple fedora 32

Per instal·lar una VM via HTTP cal indicar el directori del repositori de Fedora  que conté el
directori /images, però no aquest propi directori. Així per exemple podem navegar per totes les
versions de Fedora a dl.fedoraproject.org i navegar fins a la versió escollida, seleccionar release,
everything, l'arquitectura i os (però no la ruta dins images).

 * https://dl.fedoraproject.org/pub/fedora/linux/releases/
 * http://archives.fedoraproject.org/pub/archive/fedora/linux/releases/
 * [Fedora-32](https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/32/Everything/x86_64/os/)



### Exemple amb debian 11

A debian naveguem fins a debian dists i escollim la distribució que volem, llavors a main i seleccionar
el directori installer amb l'arquitectura que cal. Indicar la ruta fins aquest directori


 * http://ftp.us.debian.org/debian/dists/
 * [Debian 11 Bullseye](http://ftp.us.debian.org/debian/dists/Debian11.6/main/installer-amd64/)


### Exemples ubuntu 

A ubuntu naveguem fins a dists i seleccionem la versió que volem (per exemple bionic), seguit
de main i de installer-amd64 si aquesta és la arquitectura a utilitzar. En aquest directori és on hi ha el 
root installation tree necessari per fer el procediment d'instal·lació.  No cal triar res de dins,
només indicar aquesta ruta fins aquest directori.

En navegar per les versions d'ubuntu no s'identifiquen pel numero de versió sinó pel
nom de la release (18=bionic, 20=focal, 21=impish, etc) Si no tenim clar el nom de la versió
es pot seleccionar el release-notes d'una versió i allà es pot observar el número i el nom de versió).

 * [Ubuntu releases](http://archive.ubuntu.com/ubuntu/dists/) 

#### Ubuntu 18 bionic

 * http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/
 * [Ubuntu ](http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/)

#### Ubunt1 20 focal

  * http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/

