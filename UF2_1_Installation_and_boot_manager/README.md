# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


###  Virtualization and Installation 

Topic 101: System Architecture

101.1 Design hard disk layout

Topic 102: Linux Installation and Package Management

 * [HowTo-ASIX-Virtualization_and_installations.pdf](https://gitlab.com/edtasixm05/m05-hardware/-/blob/master/UF2_1_Installation_and_boot_manager/HowTo-ASIX-Virtualization_and_installations.pdf)
 * [Installation_cloud_images.pdf](https://gitlab.com/edtasixm05/m05-hardware/-/blob/master/UF2_1_Installation_and_boot_manager/LPI-102.1%20Installation_cloud_images.pdf)
 
