# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona

[ASIX-M01](https://gitlab.com/edtasixm01/m01-operatius)  |  [ASIX-M05](https://gitlab.com/edtasixm05/m05-hardware) |  [ASIX-M06](https://gitlab.com/edtasixm06/m06-aso) |  [ASIX-M11](https://gitlab.com/edtasixm11/m11-sad)

---

### UF1 Arquitectura de sistemes


#### Arquitectura i dispositius

Topic 101: System Architecture

101.1 Determine and configure hardware settings


#### Mastering Time

Topic 108: Essential System Services

108.1 Maintain system time


#### Package Management

Topic 102: Linux_installation_and_package_management

102.3 Manage shared libraries

102.4 Use Debian package management

102.5 Use RPM and YUM package management


---

### UF2 Arrancada, instal·lacions i màquines virtual


#### Boot the system

Topic 101: System Architecture

101.2 Boot the System

101.3 Change runlevels / boot targets and shutdown or reboot system


#### Installation & Boot Manager

Topic 102: Linux Installation and Package Management

102.1 Design hard disk layout

102.2 Install a boot manager


#### Virtualization

Topic 102: Linux Installation and Package Management

102.6 Linux as a virtualization guest

---

### UF3 Containers i Cloud Computing

#### Containers

Topic 102: Linux Installation and Package Management

102.6 Linux as a virtualization guest

Topic 352: Container Virtualization

352.1 Container Virtualization Concepts

352.3 Docker

352.4 Container Orchestration Platforms

