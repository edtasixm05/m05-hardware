### Exercicis del gestor de paquets DNF

Gestió de paquets i repositoris a Fedora. Instal·lació de software amb *dnf*

##### Exercici 1.

Llisteu tots els paquets instal·lats.

##### Exercici 2.

Amb quina opció `dnf` ens mostra a quin paquet pertany l’ordre `useradd`?

##### Exercici 3.

Instal·leu el paquet mc.

##### Exercici 4.

Intenteu instal·lar de nou del paquet mc.

##### Exercici 5.

Quina ordre em troba tots els paquets que continguin la cadena libreoffice?
(Filtra el langpack de català)

##### Exercici 6.

Actualitzeu (que no instal·leu) el pack de català de libreoffice a la darrera
versió. (Si ja està instal·lat català, castellà i anglès prova amb el francès: *fr*). Com actualitzaries tot el sistema?

##### Exercici 7.

Instal·leu el grup Virtualization, al menys de dues maneres diferents.

##### Exercici 8.

Llisteu tots els grups de paquets.

##### Exercici 9.

Llisteu tots els repositoris configurats

- Per defecte, que ens surt? És a dir, l'ordre que hem utilitzat ens mostra
  tots els repositoris o només els habilitats (*enabled*)?

- si els volem tots, tants els habilitats com els que no (*enabled/disabled*),
  quina seria l'ordre?

##### Exercici 10.

Instal·leu el repositori necessari per poder instal·lar després `riot` (un
client del protocol matrix)

En realitat, que hem fet instal·lant aquest repositori? Quines variables em
diuen si el respoitori està activat? Quina variable em diu a quina adreça va a
buscar els paquets del repositori?

##### Exercici 11.

Deshabiliteu el repositori anterior amb una ordre des de la consola i després
comproveu que ha canviat la variable a la que es feia referència abans.
Comproveu que no es pot instal·lar *riot*. Torneu a activar-lo després i
instal·leu riot.


