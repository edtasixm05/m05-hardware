# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


###  Boot the system

Topic 101: System Architecture

101.2 Boot the System

101.3 Change runlevels / boot targets and shutdown or reboot system



 
 * [Apunts-Exericis LPI-101.2 Boot the system](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF2_1_Boot_the_system/LPI-101.2%20Boot%20the%20system.pdf)

 * [Apunts HowTo-ASIX-Arrencada-Grub.pdf](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF2_1_Boot_the_system/HowTo-ASIX-Arrencada-Grub.pdf)

 * [LPI 101.2 Boot the system](https://learning.lpi.org/en/learning-materials/101-500/101/101.2/)
 
 * [IBM developworks 101.2 Boot the system](https://developer.ibm.com/tutorials/l-lpic1-101-2/)

 
